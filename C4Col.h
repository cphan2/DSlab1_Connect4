#ifndef C4COL_H
#define C4COL_H

/* Chau-Nhi Phan, C4Col.h
 Header file for connect 4 column class to be used to simulate a game of connect 4.
*/
class C4Col{
    public:
        C4Col(int = 6); // default constructor takes number of rows as input
        C4Col( const C4Col &); // copy constructor
        ~C4Col(); // destructor
        int isFull(); // determines if a column is full
        char getDisc(int); // returns requested element of array
        int getMaxDiscs();
        void addDisc(char);
    private:
        int maxDiscs; // maximum number of discs allowed
        int numDiscs; // number of discs currently in column
        char * Discs; // character array of discs
};

#endif
