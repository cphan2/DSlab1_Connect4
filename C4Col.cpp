/* Chau-Nhi Phan, C4Col.cpp
 *
 * Implementation for the C4Col class. Functions are for a single column of connect 4.
 */

#include <iostream>
#include "C4Col.h"
using namespace std;

C4Col::C4Col(int height) {
    numDiscs = 0; // 0 discs when initialized (empty column)
    maxDiscs = (height>0?height:6); // check for at least one row
    Discs = new char[maxDiscs]; // allocate memory for char array
    for (int i = 0; i < maxDiscs; i++) {
        Discs[i] = ' ';
    }
}

C4Col::C4Col(const C4Col &copyCol) : maxDiscs(copyCol.maxDiscs) {
    Discs = new char[maxDiscs]; // allocate memory for array

    for (int i = 0; i < maxDiscs; i++)
        Discs[i] = copyCol.Discs[i]; // copy chars into new object
}
C4Col::~C4Col() {
    delete [] Discs;
}

int C4Col::isFull() {
    if (numDiscs == maxDiscs) return 1;
    else return 0;
}

char C4Col::getDisc(int x) {
    if (x >= 0 && x < maxDiscs)
        return Discs[x];
    else cout << "Invalid entry. Please enter a number between 0 and " << maxDiscs-1 << endl;
}

int C4Col::getMaxDiscs() {
    return maxDiscs;
}

void C4Col::addDisc(char newDisc) {
    if (!isFull())
        Discs[numDiscs++] = newDisc;
    else cout << "This column is full. Please choose a different column.\n";
}
