/* Chau-Nhi Phan, C4Board.cpp
 *
 * Implementation for the C4Board class
 * Contains functions to display and play a game of Connect 4
 * Contains C4Col as a member function
 */

#include <iostream>
#include "C4Col.h"
#include "C4Board.h"

using namespace std;

C4Board::C4Board(int nCol) {
    numCols = nCol > 1 ? nCol : 7; // make sure there is more than 1 column
    Board = new C4Col[numCols]; // sllocate memory for array of C4Cols
    turn = 0;
}

C4Board::C4Board(const C4Board &copyBoard) : numCols(copyBoard.numCols) {
    Board = new C4Col[numCols];
}

C4Board::~C4Board() {
    delete [] Board;
}

void C4Board::display() {
    for (int i = Board[0].getMaxDiscs() - 1; i >= 0; i--) { // index from top to bottom
        cout << "|";
        for (int j = 0; j < numCols; j++) { // index from left to right
            cout << " " << Board[j].getDisc(i) << " |";
        }
        cout << endl;
    }

    for (int i = 1; i <= numCols; i++) {
        cout << "| " << i << " ";
    }
    cout << "|\n";
}

bool C4Board::checkBounds(int col) {
    if (col < 0 || col > numCols-1)
        return false;
    else
        return true;
}

void C4Board::play() {
    int col; // column chosen by player
    bool gameover = false; // status of the game
    while (!gameover) {
        display();
        cout << "Enter " << 2*numCols << " to quit.\n";
        cout << "Player " << (turn % 2)+1 << " please choose a column: ";
        cin >> col;

        if (col == 2*numCols) { // user quit
            cout << "Thanks for playing! Have a wonderful day!\n";
            gameover = true;
            break;
        }

        if(checkBounds(col-1)) {
            char d = ((turn%2)+1 == 1)?'X':'O'; // determine which char to use based on whose turn it is
            if (!(Board[col-1].isFull())) // only change turns when a move is valid
                turn++;

            Board[col-1].addDisc(d); // attempt to add disc whether full or not (error message displayed in addDisc method)

            if (winner()) {
                display(); // print winning board
                cout << "Congratulations Player " << ((turn+1)%2)+1 << "! You won! Go celebrate!\n";
                gameover = true;
            }
    
            if (turn >= (numCols*(Board[col-1].getMaxDiscs()))) { // nobody won and all spots are full
                display();
                cout << "Game over. It's a tie!\n";
                gameover = true;
            }
        }
        else {
            cout << "Invalid column. Please enter a column between 1 and " << numCols << ".\n";
        }   

    }

}

bool C4Board::winner() {
    int player = ((turn-1)%2)+1; // 1 or 2
    char c = (player == 1?'X':'O');
    
    // check to the right
    for (int i = 0; i <= numCols-4; i++) { // column
        for (int j = 0; j < Board[i].getMaxDiscs(); j++) { // row
            if ((Board[i].getDisc(j) == c) && (Board[i+1].getDisc(j) == c) && (Board[i+2].getDisc(j) == c) && Board[i+3].getDisc(j) == c)
                return true;
        }
    }

    // check up
    for (int j = 0; j <= (Board[0].getMaxDiscs())-4; j++) { // rows
        for (int i = 0; i < numCols; i++) { // columns
            if ((Board[i].getDisc(j) == c) && (Board[i].getDisc(j+1) == c) && (Board[i].getDisc(j+2) == c) && (Board[i].getDisc(j+3) == c))
                return true;
        }
    }

    // check up & right
    for (int i = 0; i <= numCols-4; i++) { // columns
        for (int j = 0; j <= (Board[0].getMaxDiscs())-4; j++) { // rows
            if ((Board[i].getDisc(j) == c) && (Board[i+1].getDisc(j+1) == c) && (Board[i+2].getDisc(j+2) == c) && (Board[i+3].getDisc(j+3) == c))
                return true;
        }
    }

    // check up & left
    for (int i = numCols-1; i > 2; i--) { // columns
        for (int j = 0; j <= (Board[0].getMaxDiscs())-4; j++) { // rows
            if ((Board[i].getDisc(j) == c) && (Board[i-1].getDisc(j+1) == c) && (Board[i-2].getDisc(j+2) == c) && (Board[i-3].getDisc(j+3) == c))
                return true;
        }
    }
    
    return false;
}
