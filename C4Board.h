#ifndef C4BOARD_H
#define C4BOARD_H

/* Chau-Nhi Phan, C4Board.h
 * Interface for the C4Board class
 * Makes use of the C4Col class as a member
 */

#include "C4Col.h"

class C4Board {
    public:
        C4Board(int nCol = 7); // default constructor
        C4Board(const C4Board &); // copy constructor
        ~C4Board(); // destructor
        void display(); // prints current board
        bool checkBounds(int); // check if user's column choice is valid
        void play(); // allows two players to play connect 4
    private:
        int numCols; // number of columns
        C4Col * Board; // array of C4Cols
        int turn; // keeps track of whose turn it is
        bool winner(); // determines if someone has won

};

#endif
