/* Chau-Nhi Phan, main.cpp
 * Driver file to play Connect 4 using the C4Board and C4Col classes.
 */

#include "C4Board.h"
#include <iostream>
using namespace std;

int main() {
    C4Board c4;
    c4.play();
}
