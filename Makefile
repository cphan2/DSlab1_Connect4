# Chau-Nhi Phan
# Makefile for Connect 4

playC4: main.o C4Board.o C4Col.o
	g++ main.o C4Board.o C4Col.o -o playC4

main.o: main.cpp C4Board.h
	g++ -c main.cpp

C4Board.o: C4Board.cpp C4Board.h C4Col.o
	g++ -c C4Board.cpp

C4Col.o: C4Col.h C4Col.cpp
	g++ -c C4Col.cpp

clean:
	rm *.o playC4
